---
- ansible.builtin.set_fact:
    local_binary: ""
    installed_version: ""
    desired_version: ""
    download_archive: ""
    download_signature: ""
    stopped_service: ""
    github_release: ""
    go_os: ""
    go_arch: ""
    checksum_signature_file: ""
    checksum_file: ""
    full_filename: ""
    release_checksum: ""
  tags:
    - vault-pki-cli
    - vault-pki-cli-install

- name: Check for existing binary
  stat:
    path: "/usr/local/bin/{{ binary_name }}"
  register: vault_pki_cli_local_binary
  tags:
    - vault-pki-cli
    - vault-pki-cli-install

- name: Check installed version
  block:
  - name: Check installed version
    shell: "/usr/local/bin/{{ binary_name }} version 2>&1 | awk '{print $1}'"
    register: vault_pki_cli_installed_version
    changed_when: false

  - name: Register local version
    set_fact:
      vault_pki_cli_installed_version: "{{ vault_pki_cli_installed_version.stdout }}"
    changed_when: false
  
  - debug:
      msg: "Installed version: {{ vault_pki_cli_installed_version }}"
    changed_when: false
  when: vault_pki_cli_local_binary.stat.exists
  tags:
    - vault-pki-cli
    - vault-pki-cli-install

- name: Fetch latest release information from github
  block:
  - uri:
      url: "https://api.github.com/repos/{{ github_repo }}/releases/latest"
      return_content: true
    register: github_release
    retries: 3
    delay: 5

  - name: Set github release
    set_fact:
      vault_pki_cli_desired_version: "{{ github_release.json.tag_name }}"
    changed_when: false

  - name: Print latest GitHub version
    debug:
      msg: "Latest GitHub version: {{ vault_pki_cli_desired_version }}"
    changed_when: false
  when: vault_pki_cli_desired_version is not defined or not vault_pki_cli_desired_version
  tags:
    - vault-pki-cli
    - vault-pki-cli-install

- block:
    - name: Set vars for generating download link
      set_fact:
        go_os: "{{ ansible_system | lower }}"
        go_arch: "{{ 'amd64' if ansible_architecture == 'x86_64' else 'armv6' if ansible_architecture == 'armv6l' else 'armv7' if ansible_architecture == 'armv7l' else 'aarch64' if ansible_architecture == 'aarch64' }}"
      changed_when: false
    
    - name: Set filenames
      set_fact:
        checksum_signature_file: "/tmp/{{ binary_name }}-checksums.sha256.sig"
        checksum_file: "/tmp/{{ binary_name }}-checksums.sha256"
        full_filename: "{{ binary_name }}-{{ go_os }}-{{go_arch }}"
      changed_when: false

    - name: Get checksum for archive
      set_fact:
        release_checksum: "{{ item.split(' ')[0] }}"
      when: 
        - full_filename in item
        - (not vault_pki_cli_local_binary.stat.exists or vault_pki_cli_installed_version != vault_pki_cli_desired_version)
      with_items:
        - "{{ lookup('url', 'https://github.com/{{ github_repo }}/releases/download/' + vault_pki_cli_desired_version + '/checksum.sha256', wantlist=True) | list }}"

    - name: Download binary
      become: false
      get_url:
        url: "https://github.com/{{ github_repo }}/releases/download/{{ vault_pki_cli_desired_version }}/{{ full_filename }}"
        dest: "/tmp/{{ full_filename }}"
        checksum: "sha256:{{ release_checksum }}"
      register: download_archive
      until: download_archive is succeeded
      retries: 5
      delay: 2
    
    - name: Download release checksum
      become: false
      get_url:
        url: "https://github.com/{{ github_repo }}/releases/download/{{ vault_pki_cli_desired_version }}/checksum.sha256"
        dest: "{{ checksum_file }}"
      register: vault_pki_cli_download_signature
      until: vault_pki_cli_download_signature is succeeded
      retries: 5
      delay: 2
    
    - name: Download release checksum signature
      become: false
      get_url:
        url: "https://github.com/{{ github_repo }}/releases/download/{{ vault_pki_cli_desired_version }}/checksum.sha256.sig"
        dest: "{{ checksum_signature_file }}"
      register: vault_pki_cli_download_signature
      until: vault_pki_cli_download_signature is succeeded
      retries: 5
      delay: 2

    - name: Verify signature
      command: "signify -V -p /etc/signify/soerenschneider-github.pub -m {{ checksum_file }}"
      changed_when: false
    
    - name: Stop service
      service:
        name: "{{ service_name }}"
        state: stopped
      failed_when: false
      register: stopped_service

    - name: Copy binaries
      copy:
        src: "/tmp/{{ full_filename }}"
        dest: "/usr/local/bin/{{ item }}"
        remote_src: yes
        mode: 0755
        owner: root
        group: root
      with_items:
        - "{{ binary_name }}"

  always:
    - name: Delete downloaded files
      file:
        state: absent
        path: "{{ item }}"
      with_items:
        - "{{ checksum_signature_file }}"
        - "{{ checksum_file }}"
        - "/tmp/{{ full_filename }}"
      failed_when: false

    - name: Restart service
      service:
        name: "{{ service_name }}"
        state: restarted
      when: stopped_service is defined and stopped_service is changed

  when: not vault_pki_cli_local_binary.stat.exists or vault_pki_cli_desired_version != vault_pki_cli_installed_version
  tags:
    - vault-pki-cli
    - vault-pki-cli-install
