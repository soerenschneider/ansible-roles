#!/bin/sh

# exit code 0 -> certs did not need replacement
# exit code 1 -> error
# exit code 2 -> dest certs were replaced

set -ue

if [ $# -ne 3 ]; then
	echo "Expecting three params: domain src dest"
	exit 1
fi

DOMAIN="$1"
SRC="$(realpath "$2")"
DEST="$(realpath "$3")"

check_file() {
	if [ -f "${1}/${DOMAIN}.key" ]; then
	    md5sum "${1}/${DOMAIN}.key"
    else
        echo "NOSUCHFILE"
    fi
}

get_expiry_time() {
    EXPIRY_DATE=$(openssl x509 -noout -dates -in "$1/${DOMAIN}.cer" 2> /dev/null | grep -i ^notAfter | cut -d'=' -f2)
    date --date="${EXPIRY_DATE}" +%s
}

copy_certs() {
    if [ ! -d "${DEST}" ]; then
        mkdir -p "${DEST}"
        chown root:root "${DEST}"
        chmod 700 "${DEST}"
    fi

    cp "${SRC}/fullchain.cer" "${DEST}/${DOMAIN}.cer"
    cp "${SRC}/${DOMAIN}.key" "${DEST}/${DOMAIN}.key"
    exit 2
}

flow() {
    CHKSUM_SRC=$(check_file "${SRC}")
    CHKSUM_DEST=$(check_file "${DEST}")
    
    if [ "${CHKSUM_DEST}" = "NOSUCHFILE" ] && [ "${CHKSUM_SRC}" != "${CHKSUM_DEST}" ]; then
        copy_certs
    fi

    if [ "${CHKSUM_SRC}" != "NOSUCHFILE" ] && [ "${CHKSUM_SRC}" = "${CHKSUM_DEST}" ]; then
        exit 0
    fi

    local EXPIRY_SRC=$(get_expiry_time "${SRC}")
    local EXPIRY_DEST=$(get_expiry_time "${DEST}")

    if [ ${EXPIRY_SRC} -gt ${EXPIRY_DEST} ]; then
        copy_certs
    fi

    exit 0
}

flow
