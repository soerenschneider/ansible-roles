---
- name: Add user acme
  user:
    name: "{{ acmesh_user }}"
    system: true
  register: _acmesh_user_task
  tags:
    - acme.sh

- name: Set home dir perms
  file:
    state: directory
    path: "{{ _acmesh_user_task.home }}"
    owner: "{{ acmesh_user }}"
    group: "{{ acmesh_user }}"
    mode: "0700"
  tags:
    - acme.sh

- name: Add credentials
  template:
    src: aws-credentials.j2
    dest: "{{ acmesh_credentials_env_file }}"
    owner: "{{ acmesh_user }}"
    group: "{{ acmesh_user }}"
    mode: "0400"
  tags:
    - acme.sh

- name: Check for existing acmesh binary
  stat:
    path: "{{ _acmesh_user_task.home }}/.acme.sh/acme.sh"
  register: acmesh_binary
  tags:
    - acme.sh

- name: Check installed version
  become_user: "{{ acmesh_user }}"
  block:
    - shell: "{{ _acmesh_user_task.home }}/.acme.sh/acme.sh --version | grep ^v"
      register: acmesh_installed_version

    - set_fact:
        acmesh_installed_version: "{{ acmesh_installed_version.stdout }}"

  when: acmesh_binary.stat.exists
  tags:
    - acme.sh

- name: Fetch latest release information from github
  become_user: "{{ acmesh_user }}"
  block:
    - name: Get release info
      uri:
        url: https://api.github.com/repos/acmesh-official/acme.sh/releases/latest
        return_content: true
      register: github_release
      retries: 3
      delay: 5
      until: github_release is not failed

    - name: Set acmesh release
      set_fact:
        acmesh_version: "{{ github_release.json.tag_name | regex_replace('^v?(.*)$', '\\1') }}"
  when: acmesh_version is not defined or not acmesh_version
  tags:
    - acme.sh

- name: Print version info
  debug:
    msg: "Latest acmesh release: {{ acmesh_version }}, locally installed: {{ acmesh_installed_version }}"
  tags:
    - acme.sh

- block:
    - name: Checkout repo
      become_user: "{{ acmesh_user }}"
      git:
        repo: https://github.com/acmesh-official/acme.sh.git
        dest: /tmp/acme.sh

    - name: Install
      become_user: "{{ acmesh_user }}"
      command: "./acme.sh --install -m {{ acmesh_email }}"
      args:
        chdir: /tmp/acme.sh
    
    - name: Delete repo
      file:
        path: /tmp/acme.sh
        state: absent
  when:
    - not acmesh_binary.stat.exists
  tags:
    - acme.sh

- name: Upgrade script
  become_user: "{{ acmesh_user }}"
  command: "./acme.sh --upgrade"
  args:
    chdir: "{{ _acmesh_user_task.home }}/.acme.sh"
  when: acmesh_binary.stat.exists and acmesh_version and acmesh_version != acmesh_installed_version
  tags:
    - acme.sh

- name: Copy cert_copy script
  copy:
    src: files/cert-sync.sh
    dest: /usr/local/bin/cert-sync
    owner: root
    group: root
    mode: "0755"
    remote_src: false
  tags:
    - acme.sh

- name: Issue cert
  become_user: "{{ acmesh_user }}"
  command: "{{ _acmesh_user_task.home }}/.acme.sh/acme.sh --issue --dns dns_aws --keylength 4096 --server letsencrypt {% for domain in acmesh_domains %}-d {{ domain }} {% endfor %}"
  environment:
    AWS_ACCESS_KEY_ID: "{{ acmesh_aws_access_key }}"
    AWS_SECRET_ACCESS_KEY: "{{ acmesh_aws_secret_key }}"
  failed_when: false
  tags:
    - acme.sh

- name: Add renewal service
  ansible.builtin.template:
    src: "acmesh-renewal.{{ item }}.j2"
    dest: "/etc/systemd/system/acmesh-renewal.{{ item }}"
    owner: root
    group: root
    mode: "0644"
  with_items:
    - service
    - timer
  tags:
    - acme.sh
    - acme.sh-renewal

- name: Enable renewal service
  ansible.builtin.systemd:
    name: "acmesh-renewal.{{ item }}"
    enabled: true
    state: started
    daemon_reload: true
  with_items:
    - service
    - timer
  tags:
    - acme.sh
    - acme.sh-renewal
