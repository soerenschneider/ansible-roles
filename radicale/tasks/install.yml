---
- name: create radicale system group
  group:
    name: "{{ radicale_group }}"
    system: true
    state: present
  tags:
    - radicale
    - radicale-install

- name: create radicale system user
  user:
    name: "{{ radicale_user }}"
    system: true
    shell: "/sbin/nologin"
    group: "{{ radicale_group }}"
    home: /
    createhome: false
  tags:
    - radicale
    - radicale-install

- name: create config dir
  file:
    path: "{{ radicale_config_dir }}"
    state: directory
    owner: "{{ radicale_user }}"
    group: "{{ radicale_group }}"
    mode: 0750
  tags:
    - radicale
    - radicale-install

- name: create data dir
  file:
    path: "{{ radicale_data_dir }}"
    state: directory
    owner: "{{ radicale_user }}"
    group: "{{ radicale_group }}"
    mode: 0750
  tags:
    - directories
    - radicale

- name: create radicale dir
  file:
    path: "{{ radicale_dir }}"
    state: directory
    owner: "{{ radicale_user }}"
    group: "{{ radicale_group }}"
    mode: 0755
  tags:
    - directories
    - radicale

- name: install requirements
  package:
    name:
      - python3
      - python3-pip
      - git
  tags:
    - radicale
    - radicale-install

- name: install requirements
  apt:
    name:
      - python3
      - python3-pip
      - python3-dev
      - python3-venv
      - python3-setuptools
      - libffi-dev 
      - libssl-dev
      - libcoap2
      - git
  when: ansible_os_family == 'Debian'
  tags:
    - radicale
    - radicale-install

  # tries up to 3x to fetch latest version from pypi. if it fails, we don't
  # tag a certain version when installing/upgrading via pip
- name: get latest version info from pypi
  uri:
    url: https://pypi.org/pypi/Radicale/json 
    return_content: yes
  register: json_response
  retries: 3
  delay: 5
  when: radicale_version | length < 1
  ignore_errors: yes
  tags:
    - radicale
    - radicale-install

- name: extract version from response
  set_fact: 
    radicale_version: "{{ (json_response.content|from_json)['info']['version'] }}"
  when: json_response is defined
  tags:
    - radicale
    - radicale-install

- name: "install radicale version v{{ radicale_version }}"
  become: yes
  become_user: "{{ radicale_user }}"
  pip:
    name: "radicale[bcrypt]{% if radicale_version | length > 0 %}=={{ radicale_version }}{% endif %}"
    virtualenv_command: /usr/bin/python3 -m venv
    virtualenv: "{{ radicale_dir }}"
    extra_args: --upgrade
  notify:
    - restart radicale
  tags:
    - radicale
    - radicale-install
