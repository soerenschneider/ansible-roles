---
- name: Check for latest version
  uri:                                                               
    url: https://api.github.com/repos/google/mtail/releases/latest
    return_content: true
  register: mtail_latest 
  when: mtail_version is not defined or not mtail_version
  tags:
    - mtail
    - mtail-install

- name: Extract version from json...
  set_fact:
    mtail_version: "{{ mtail_latest.json.tag_name }}"
  when: mtail_version is not defined or not mtail_version
  tags:
    - mtail
    - mtail-install

- stat:
    path: "/usr/local/bin/mtail_{{ mtail_version }}"
  register: mtail_latest_binary
  tags:
    - mtail
    - mtail-install

- debug:
    msg: "Is desired version {{ mtail_version }} installed?: {{ mtail_latest_binary.stat.exists }}"
  tags:
    - mtail
    - mtail-install

- name: Download mtail Linux binary
  become: false
  get_url:
    url: "https://github.com/google/mtail/releases/download/{{ mtail_version }}/mtail_{{ mtail_version }}_linux_amd64"
    dest: "/tmp/mtail_{{ mtail_version }}"
  register: _download_archive
  until: _download_archive is succeeded
  retries: 5
  delay: 2
  when: not mtail_latest_binary.stat.exists and ansible_system == 'Linux'
  tags:
    - mtail
    - mtail-install

- name: "Download mtail OpenBSD binary ({{ mtail_version }})"
  become: false
  get_url:
    url: "https://github.com/it-compiles-lets-sell-it/mtail/releases/download/{{ mtail_version }}/mtail_{{ mtail_version }}_openbsd_amd64"
    dest: "/tmp/mtail_{{ mtail_version }}"
  register: _download_archive
  until: _download_archive is succeeded
  retries: 5
  delay: 2
  when: not mtail_latest_binary.stat.exists and ansible_system == 'OpenBSD'
  tags:
    - mtail
    - mtail-install

- name: Copy binary
  copy:
    src: "/tmp/mtail_{{ mtail_version }}"
    dest: "/usr/local/bin/"
    remote_src: yes 
    mode: 0755
    owner: root
    group: "{% if ansible_system == 'OpenBSD' %}wheel{% else %}root{% endif %}"
  when: not mtail_latest_binary.stat.exists
  tags:
    - mtail
    - mtail-install

- name: Delete downloaded binary
  file:
    state: absent
    path: "/tmp/mtail_{{ mtail_version }}/"
  tags:
    - mtail
    - mtail-install

- name: Create symlinks to most-recent version
  file:
    src: "/usr/local/bin/mtail_{{ mtail_version }}"
    dest: "/usr/local/bin/mtail"
    owner: root
    group: "{% if ansible_system == 'OpenBSD' %}wheel{% else %}root{% endif %}"
    state: link
  when: not mtail_latest_binary.stat.exists
  tags:
    - mtail
    - mtail-install
