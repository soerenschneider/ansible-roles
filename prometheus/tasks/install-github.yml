---
- name: Check for existing prometheus binary
  stat:
    path: "/usr/local/bin/prometheus"
  register: prometheus_binary
  tags:
    - monitoring
    - prometheus
    - prometheus-install
    - deps

- name: Check installed version
  block:
  - shell: "/usr/local/bin/prometheus --version 2>&1 | head -n1 | awk '{print $3}'"
    register: prometheus_installed_version

  - set_fact:
      prometheus_installed_version: "v{{ prometheus_installed_version.stdout }}"
  
  - debug:
      msg: "Installed version: {{ prometheus_installed_version }}, wanted: {{ prometheus_version }}"
  when: prometheus_binary.stat.exists
  tags:
    - monitoring
    - prometheus
    - prometheus-install
    - deps

- name: "Install prometheus {{ prometheus_version }}"
  block:
  - name: "Set OS for generating download link"
    set_fact:
      go_os: "{{ ansible_system | lower }}"
      go_arch: "{{ 'amd64' if ansible_architecture == 'x86_64' else 'armv6' if ansible_architecture == 'armv6l' else 'armv7' if ansible_architecture == 'armv7l' else 'arm64' if ansible_architecture == 'aarch64' }}"

  - name: Get checksum for archive
    set_fact:
      prometheus_checksum: "{{ item.split(' ')[0] }}"
    with_items:
      - "{{ lookup('url', 'https://github.com/prometheus/prometheus/releases/download/' + prometheus_version + '/sha256sums.txt', wantlist=True) | list }}"
    when: "((go_os) + '-' + (go_arch) + '.tar.gz') in item and (not prometheus_binary.stat.exists or prometheus_installed_version != prometheus_version)"

  - name: Download prometheus archive
    become: false
    get_url:
      url: "https://github.com/prometheus/prometheus/releases/download/{{ prometheus_version }}/prometheus-{{ prometheus_version[1:] }}.{{ go_os }}-{{ go_arch }}.tar.gz"
      dest: "/tmp/prometheus-{{ prometheus_version }}.{{ go_os }}-{{ go_arch }}.tar.gz"
      checksum: "sha256:{{ prometheus_checksum }}"
    register: download_archive
    until: download_archive is succeeded
    retries: 5
    delay: 2

  - name: Unpack binaries from archive
    become: false
    unarchive:
      src: "/tmp/prometheus-{{ prometheus_version }}.{{ go_os }}-{{ go_arch }}.tar.gz"
      dest: "/tmp"
      creates: "/tmp/prometheus-{{ prometheus_version[1:] }}.{{ go_os }}-{{ go_arch }}/prometheus"
      remote_src: yes
    check_mode: false

  - name: Copy binaries
    copy:
      src: "/tmp/prometheus-{{ prometheus_version[1:] }}.{{ go_os }}-{{ go_arch }}/{{ item }}"
      dest: "/usr/local/bin/{{ item }}"
      remote_src: yes
      mode: 0755
      owner: root
      group: root
    with_items:
      - prometheus
      - promtool
    notify:
      - restart prometheus

  - name: Delete downloaded files
    file:
      state: absent
      path: "{{ item }}"
    with_items:
      - "/tmp/prometheus-{{ prometheus_version[1:] }}.{{ go_os }}-{{ go_arch }}"
      - "/tmp/prometheus-{{ prometheus_version }}.{{ go_os }}-{{ go_arch }}.tar.gz"
  when: 
    - not prometheus_binary.stat.exists or prometheus_version != prometheus_installed_version
  tags:
    - monitoring
    - prometheus
    - prometheus-install
    - deps
