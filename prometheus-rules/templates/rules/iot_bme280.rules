groups:
- name: TemperatureSensor
  rules:
  - alert: RoomTemperatureTooLow
    expr: iot_sensors_bme280_temp_celsius < {{ prometheus_rules.iot_sensors_bme280.temp.low_warning | default(18)}}
    for: 1h
    labels:
      severity: warning
    annotations:
      summary: Measured temperature {% raw %}of {{ $value | printf "%.1f" }}C° at "{{ $labels.location }}"{% endraw %} dropped below 18C°.
  - alert: RoomTemperatureFreezing
    expr: iot_sensors_bme280_temp_celsius < {{ prometheus_rules.iot_sensors_bme280.temp.low_critical | default(15) }}
    for: 1h
    labels:
      severity: critical
    annotations:
      summary: Measured temperature {% raw %}of {{ $value | printf "%.1f" }}C° at "{{ $labels.location }}"{% endraw %} dropped below 16C°.
  - alert: HighHumidity
    expr: iot_sensors_bme280_humidity_percent > {{ prometheus_rules.iot_sensors_bme280.humdity.high_warning | default(60) }}
    for: 8h
    labels:
      severity: warning
    annotations:
      summary: Measured humdity {% raw %}of {{ $value | printf "%.0f" }}% at "{{ $labels.location }}"{% endraw %} above 60%.
  - alert: ExtremelyHighHumidity
    expr: iot_sensors_bme280_humidity_percent > {{ prometheus_rules.iot_sensors_bme280.humdity.high_critical | default(70) }}
    for: 1h
    labels:
      severity: critical
    annotations:
      summary: Measured humdity {% raw %}of {{ $value | printf "%.0f" }}% at "{{ $labels.location }}"{% endraw %} above 70%.
  - alert: LowHumidity
    expr: iot_sensors_bme280_humidity_percent < 40
    for: 1h
    labels:
      severity: warning
    annotations:
      summary: Measured humdity {% raw %}of {{ $value | printf "%.0f" }}% at "{{ $labels.location }}"{% endraw %} dropped below 40%.
  - alert: ExtremelyLowHumidity
    expr: iot_sensors_bme280_humidity_percent < 30
    for: 1h
    labels:
      severity: critical
    annotations:
      summary: Measured humdity {% raw %}of {{ $value | printf "%.0f" }}% at "{{ $labels.location }}"{% endraw %} dropped below 30%.
