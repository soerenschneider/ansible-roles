groups:
- name: home-assistant
  rules:
  - alert: HomeAssistantDeviceBatteryLevelCritical
    expr: homeassistant_sensor_battery_percent <= 3
    labels:
      severity: critical
    annotations:
      summary: Battery level for {% raw %}{{ $labels.friendly_name }}{% endraw %} is critical

  - alert: HomeAssistantDeviceBatteryLevel
    expr: predict_linear(homeassistant_sensor_battery_percent[7d], 7 * 24 * 3600) < 0
    for: 2h
    labels:
      severity: warning
    annotations:
      summary: Battery for {% raw %}{{ $labels.friendly_name }}{% endraw %} will be drained in less than a week

  - alert: HomeAssistantUpdateAvailable
    expr: homeassistant_binary_sensor_state{entity="binary_sensor.updater"} > 0
    for: 5m
    labels:
      severity: warning
    annotations:
      summary: Update for home assistant waiting to be installed.

  - alert: HomeAssistantLightsOnForTooLong
    expr: homeassistant_light_brightness_percent > 0
    for: 12h
    labels:
      severity: warning
    annotations:
      summary: Light {% raw %}{{ $labels.friendly_name }}{% endraw %} switched on for too long
