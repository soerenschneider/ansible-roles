---
groups:
  - name: haproxy
    rules:
      - alert: HaproxyServerHealthcheckFailure
        expr: increase(haproxy_server_check_failures_total[1m]) > 0
        for: 1m
        labels:
          severity: warning
        annotations:
          summary: HAProxy server healthcheck failure (instance {% raw %}{{ $labels.instance }}{% endraw %})

      - alert: HaproxyHasNoAliveBackends
        expr: haproxy_backend_active_servers{proxy!="stats"} + haproxy_backend_backup_servers{proxy!="stats"} == 0
        for: 1m
        labels:
          severity: critical
        annotations:
          summary: "{% raw %}HAproxy has no alive backends (instance {{ $labels.instance }}){% endraw %}"
          description: "{% raw %}HAProxy has no alive active or backup backends for {{ $labels.proxy }}\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}{% endraw %}"

      - alert: HaproxyBackendMaxActiveSession>80%
        expr: ((haproxy_server_max_sessions >0) * 100) / (haproxy_server_limit_sessions > 0) > 80
        for: 2m
        labels:
          severity: warning
        annotations:
          summary: "{% raw %}HAProxy backend max active session > 80% (instance {{ $labels.instance }}){% endraw %}"
          description: "{% raw %}Session limit from backend {{ $labels.proxy }} to server {{ $labels.server }} reached 80% of limit - {{ $value | printf \"%.2f\"}}%\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}{% endraw %}"

      - alert: HaproxyBackendConnectionErrors
        expr: (sum by (proxy) (rate(haproxy_backend_connection_errors_total[1m]))) > 100
        for: 1m
        labels:
          severity: critical
        annotations:
          summary: "{% raw %}HAProxy backend connection errors (instance {{ $labels.instance }}){% endraw %}"
          description: "{% raw %}Too many connection errors to {{ $labels.fqdn }}/{{ $labels.backend }} backend (> 100 req/s). Request throughput may be too high.\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}{% endraw %}"

      - alert: HaproxyServerConnectionErrors
        expr: (sum by (proxy) (rate(haproxy_server_connection_errors_total[1m]))) > 100
        for: 1m
        labels:
          severity: critical
        annotations:
          summary: "{% raw %}HAProxy server connection errors (instance {{ $labels.instance }}){% endraw %}"
          description: "{% raw %}Too many connection errors to {{ $labels.server }} server (> 100 req/s). Request throughput may be too high.\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}{% endraw %}"

      - alert: HaproxyHighHttp4xxErrorRateBackend
        expr: ((sum by (proxy) (rate(haproxy_server_http_responses_total{code="4xx"}[1m])) / sum by (proxy) (rate(haproxy_server_http_responses_total[1m]))) * 100) > 5
        for: 1m
        labels:
          severity: critical
        annotations:
          summary: "{% raw %}HAProxy high HTTP 4xx error rate backend (instance {{ $labels.instance }}){% endraw %}"
          description: "{% raw %}Too many HTTP requests with status 4xx (> 5%) on backend {{ $labels.fqdn }}/{{ $labels.backend }}\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}{% endraw %}"

      - alert: HaproxyHighHttp5xxErrorRateBackend
        expr: ((sum by (proxy) (rate(haproxy_server_http_responses_total{code="5xx"}[1m])) / sum by (proxy) (rate(haproxy_server_http_responses_total[1m]))) * 100) > 5
        for: 1m
        labels:
          severity: critical
        annotations:
          summary: "{% raw %}HAProxy high HTTP 5xx error rate backend (instance {{ $labels.instance }}){% endraw %}"
          description: "{% raw %}Too many HTTP requests with status 5xx (> 5%) on backend {{ $labels.fqdn }}/{{ $labels.backend }}\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}{% endraw %}"

      - alert: HaproxyHighHttp4xxErrorRateServer
        expr: ((sum by (server) (rate(haproxy_server_http_responses_total{code="4xx"}[1m])) / sum by (server) (rate(haproxy_server_http_responses_total[1m]))) * 100) > 5
        for: 1m
        labels:
          severity: critical
        annotations:
          summary: "{% raw %}HAProxy high HTTP 4xx error rate server (instance {{ $labels.instance }}){% endraw %}"
          description: "{% raw %}Too many HTTP requests with status 4xx (> 5%) on server {{ $labels.server }}\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}{% endraw %}"

      - alert: HaproxyHighHttp5xxErrorRateServer
        expr: ((sum by (server) (rate(haproxy_server_http_responses_total{code="5xx"}[1m])) / sum by (server) (rate(haproxy_server_http_responses_total[1m]))) * 100) > 5
        for: 1m
        labels:
          severity: critical
        annotations:
          summary: "{% raw %}HAProxy high HTTP 5xx error rate server (instance {{ $labels.instance }}){% endraw %}"
          description: "{% raw %}Too many HTTP requests with status 5xx (> 5%) on server {{ $labels.server }}\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}{% endraw %}"

      - alert: HaproxyServerResponseErrors
        expr: (sum by (server) (rate(haproxy_server_response_errors_total[1m])) / sum by (server) (rate(haproxy_server_http_responses_total[1m]))) * 100 > 5
        for: 1m
        labels:
          severity: critical
        annotations:
          summary: "{% raw %}HAProxy server response errors (instance {{ $labels.instance }}){% endraw %}"
          description: "{% raw %}Too many response errors to {{ $labels.server }} server (> 5%).\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}{% endraw %}"
