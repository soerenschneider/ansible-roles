groups:
- name: prometheus
  rules:
  - alert: Exporter can not be scraped
    expr: up == 0
    for: 15m
    labels:
      severity: critical
    annotations:
      summary: Instance {% raw %}{{ $labels.instance }}{% endraw %} could not be scraped for 15m

  - alert: PrometheusConfigurationReload
    expr: prometheus_config_last_reload_successful != 1
    for: 5m
    labels:
      severity: error
    annotations:
      summary: "Prometheus configuration reload (instance {% raw %}{{ $labels.instance }}{% endraw %})"
      description: "Prometheus configuration reload error\n  VALUE = {% raw %}{{ $value }}\n  LABELS: {{ $labels }}{% endraw %}"

  - alert: PrometheusTemplateErrors
    expr: rate(prometheus_template_expand_failures_total[1m]) > 0
    for: 5m
    labels:
      severity: error
    annotations:
      summary: "Prometheus template expansion errors"
      description: "Prometheus instance {% raw %}{{ $labels.instance }} seeing template expansion errors{% endraw %}"

  - alert: HighRemoteWriteLatency
    expr: time() - sum by (location) (prometheus_remote_storage_highest_timestamp_in_seconds{job="prometheus"}) > 60
    for: 15m
    annotations:
      description: "Prometheus remote write latency too high on instance {% raw %}{{ $labels.instance }}, location {{ $labels.location }}{% endraw %})"
      summary: Prometheus remote write latency too high
    labels:
      severity: error

  - alert: dullmonitor not running
    expr: time() - dullmonitor_heartbeat_seconds > 90
    for: 5m
    labels:
      severity: critical
    annotations:
      summary: Dullmonitor heartbeat stopped {% raw %}{{ $value | humanize}} ago{% endraw %})
