---
- name: Check for existing binary
  stat:
    path: /usr/local/bin/gocryptfs
  register: gocryptfs_binary
  tags: [gocryptfs, gocryptfs-install, deps]

- block:
  - name: Check installed version
    shell: "/usr/local/bin/gocryptfs -version | awk '{print $2}'"
    register: gocryptfs_installed_version

  - set_fact:
      gocryptfs_installed_version: "{{ gocryptfs_installed_version.stdout }}"
  
  - debug:
      msg: "Installed version: {{ gocryptfs_installed_version }}"
  when: gocryptfs_binary.stat.exists
  tags: [gocryptfs, gocryptfs-install, deps]

- block:
  - name: Fetch latest release information from github
    uri:
      url: https://api.github.com/repos/rfjakob/gocryptfs/releases/latest
      return_content: true
    register: github_release
    retries: 3
    delay: 5
    until: github_release is not failed

  - name: Set gocryptfs release
    set_fact:
      gocryptfs_version: "{{ github_release.json.tag_name }}"
  when: gocryptfs_version is not defined or gocryptfs_version | length == 0
  tags: [gocryptfs, gocryptfs-install, deps]

- name: "Set OS for generating download link"
  set_fact:
    go_os: "{{ ansible_system | lower }}"
    go_arch: "{{ 'amd64' if ansible_architecture == 'x86_64' else 'armv6' if ansible_architecture == 'armv6l' else 'armv7' if ansible_architecture == 'armv7l' }}"
  tags: [gocryptfs, gocryptfs-install, deps]

- block:
    - name: Create download directory
      become: false
      file:
        path: /tmp/gocryptfs
        state: directory
        mode: "0700"

    - name: Set archive name
      set_fact:
        gocryptfs_archive: "gocryptfs_{{ gocryptfs_version }}_linux-static_amd64.tar.gz"

    - name: Set download url
      set_fact:
        gocryptfs_url: "https://github.com/rfjakob/gocryptfs/releases/download/{{ gocryptfs_version }}/{{ gocryptfs_archive }}"

    - name: Download gocryptfs archive
      become: false
      get_url:
        url: "{{ gocryptfs_url }}"
        dest: "/tmp/gocryptfs/{{ gocryptfs_archive }}"
      register: download_gocryptfs
      until: download_gocryptfs is succeeded
      retries: 5
      delay: 2

    - name: Download gocryptfs checksum
      become: false
      get_url:
        url: "{{ gocryptfs_url }}.asc"
        dest: "/tmp/gocryptfs/{{ gocryptfs_archive }}.asc"
      register: download_gocryptfs_checksum
      until: download_gocryptfs_checksum is succeeded
      retries: 5
      delay: 2

    - name: Verify archive
      become: false
      command: "gpg2 --verify /tmp/gocryptfs/{{ gocryptfs_archive }}.asc"

    - name: Unpack binaries from archive
      become: false
      unarchive:
        src: "/tmp/gocryptfs/{{ gocryptfs_archive }}"
        dest: "/tmp/gocryptfs/"
        creates: "/tmp/gocryptfs/gocryptfs"
        remote_src: yes
      check_mode: false

    - name: Copy binaries
      copy:
        src: "/tmp/gocryptfs/gocryptfs"
        dest: "/usr/local/bin/gocryptfs"
        remote_src: yes
        mode: 0755
        owner: root
        group: root
      with_items:
        - gocryptfs

    - name: Delete downloaded files
      file:
        state: absent
        path: "{{ item }}"
      with_items:
        - "/tmp/gocryptfs"
  when: not gocryptfs_binary.stat.exists or gocryptfs_version != gocryptfs_installed_version
  tags: [gocryptfs, gocryptfs-install, deps]
