---
- name: create hass system group
  ansible.builtin.group:
    name: "{{ hass_group }}"
    system: true
    state: present
  tags:
    - hass
    - hass-install

- name: create hass system user
  ansible.builtin.user:
    name: "{{ hass_user }}"
    system: true
    shell: "/sbin/nologin"
    group: "{{ hass_group }}"
    createhome: true
  tags:
    - hass
    - hass-install

- name: Make sure log directory exists
  ansible.builtin.file:
    path: "{{ hass_log.dir }}"
    owner: "{{ hass_user }}"
    group: "{{ hass_group }}"
    mode: "0750"
    state: directory
  when: hass_log.dir is defined
  tags:
    - hass
    - hass-install

- name: install additional packages
  ansible.builtin.package:
    name:
      - netcat
  tags:
    - hass
    - hass-install

- name: install requirements
  ansible.builtin.dnf:
    name:
      - python3
      - python3-pip
      - git
  when: ansible_distribution == 'CentOS' and ansible_distribution_version >= '8'
  tags:
    - hass
    - hass-install

- name: install requirements
  ansible.builtin.apt:
    name:
      - python3
      - python3-pip
      - python3-dev
      - python3-venv
      - python3-setuptools
      - libffi-dev
      - libssl-dev
      - libxml2-dev
      - libxslt-dev
      - libcoap2
      - git
      - libjpeg-dev
      - zlib1g-dev
      - autoconf
      - build-essential
      - libopenjp2-7
      - libtiff5
  when: ansible_os_family == 'Debian'
  tags:
    - hass
    - hass-install

- name: create hass data directory
  ansible.builtin.file:
    path: "{{ hass_dir }}"
    state: directory
    owner: "{{ hass_user }}"
    group: "{{ hass_group }}"
    mode: 0755
  tags:
    - hass
    - hass-install

  # tries up to 3x to fetch latest version from pypi. if it fails, we don't
  # tag a certain version when installing/upgrading via pip
- name: get latest version info from pypi
  ansible.builtin.uri: 
    url: https://pypi.org/pypi/homeassistant/json 
    return_content: yes
  register: json_response
  retries: 3
  delay: 5
  when: hass_version | length < 1
  ignore_errors: yes
  tags:
    - hass
    - hass-install

- name: extract version from response
  ansible.builtin.set_fact: 
    hass_version: "{{ (json_response.content|from_json)['info']['version'] }}"
  when: json_response is changed
  tags:
    - hass
    - hass-install

- name: "install hass version {{ hass_version }}"
  become: yes
  become_user: "{{ hass_user }}"
  ansible.builtin.pip:
    name: "homeassistant{% if hass_version | length > 0 %}=={{ hass_version }}{% endif %}"
    virtualenv_command: /usr/bin/python3 -m venv
    virtualenv: "{{ hass_dir }}"
    extra_args: --upgrade
  notify:
    - restart hass
  tags:
    - hass
    - hass-install
